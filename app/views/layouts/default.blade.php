<html>
@include('partials.head')
<body>
    @yield('content')

    @include('partials.scripts')
    @yield('page-scripts')
</body>
</html>