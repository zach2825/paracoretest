<!-- it's probably best you don't mess with the form open tag. I didn't do anything to it, you have my word -->
{{ Form::open(['url'=>'http://submissions.paracore.com/apply']) }}
    <div class="form-group">
        {{ Form::label('name', 'Name') }}
        {{ Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Please provide your full name']) }}
    </div>
    <div class="form-group">
        {{ Form::label('email', 'Email') }}
        {{ Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'Please provide an Email address']) }}
    </div>
    <div class="form-group">
        {{ Form::label('comments', 'Additional Comments') }}
        {{ Form::textarea('comments', null, ['class' => 'form-control']) }}
    </div>
    <div class="form-group">
        <!-- a label?! That's not going to submit anything... better make this guy a submit button -->
        {{ Form::submit('Submit Application', ['class' => 'btn btn-primary'])}}
    </div>
{{ Form::close() }}