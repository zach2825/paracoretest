@extends('layouts.default')

@section('content')

<div class="container">
    <div class="row">
        <h1>PHP Developer Application</h1>
    </div>

    <div class="row">
        <div class="col-sm-12 col-md-5">
            <h4>Position Overview</h4>
            <p>ParaCore’s next Killer PHP Developer will be responsible for supporting existing website projects, developing new projects and being an all around killer developer. Out killer team prides ourselves on the quality of our work, advanced systems integration and creating user experiences that are measurable, beautiful and effective.</p>

            <p>ParaCore (www.paracore.com) is a growing interactive and digital agency specializing in creating beautiful, yet technical online campaigns and assets for our clients.  Our team capitalizes on trending technologies of which we're currently focused on the Laravel framework, responsive development, web based apps and integrating various online systems through APIs.</p>

            <p>The ideal candidate must display excellent development and communications skills with demonstrated interpersonal and organization abilities. This individual must be able to work in a fast paced environment as well as possess the ability to adapt to shifting deadlines and priorities.</p>

            <h5>Essential Duties and Responsibilities for being Killer</h5>
            <ul>
                <li>Develop new web applications and websites</li>
                <li>Maintain, enhance and upgrade existing websites</li>
                <li>Conduct user interface testing</li>
                <li>Execute browser compatibility testing</li>
                <li>Develop database-driven web interfaces for rapid, real-time site updates</li>
                <li>Update website content, images, links, files, etc</li>
                <li>Proficient in Internet related applications such as E-Mail clients, FTP clients and Web Browsers</li>
                <li>Candidate must be a team player and willing to teach and to learn</li>
                <li>Attend client meetings to gather website specifications and functional requirements</li>
             </ul>

            <h5>Required Kill Skills</h5>
            <ul>
                <li>Excellent skills in PHP</li>
                <li>Excellent skills in MySQL</li>
                <li>Strong understanding of Javascript & jQuery</li>
                <li>Strong understanding of HTML, CSS & HTML5</li>
                <li>Exceptional attention to detail in all facets (development, design, etc)</li>
                <li>Git repository system</li>
                <li>Ability to work additional hours/special projects as needed</li>
                <li>Nunchuku skills, bow hunting skills, computer hacking skills</li>
            </ul>

            <h5>Highly Preferred Specialties</h5>
            <strong>Individuals demonstrating the following skills will be given highest priority:</strong>
            <ul>
                <li>Familiarity with the Laravel framework</li>
                <li>Experience working in other MVC frameworks</li>
                <li>Advanced Google Analytics Integration and Reporting</li>
                <li>Advanced Search Engine Optimization</li>
                <li>Experience managing projects and interfacing with clients</li>
                <li>Experience integrating various web systems and API integrations</li>
                <li>Implementing A/B Testing analysis</li>
                <li>Strong understanding of user interface concepts and best practices</li>
                <li>Linux based operating systems and command line operations</li>
                <li>Apache web server</li>
                <li>Previous experience with Wordpress, Joomla, Cake PHP, Drupal or other development frameworks</li>
                <li>Mobile Development</li>
                <li>Extensive knowledge of SEO</li>
                <li>Flash video/animation experience</li>
                <li>Photoshop or Illustrator experience</li>
                <li>Social Media integration and associated development</li>
            </ul>

             <h5>Our Killer Environment</h5>
             <ul>
                <li>Small development company where every move makes a difference</li>
                <li>Competitive compensation package (PTO, health insurance, company bonus, life, vision, education allowance)</li>
                <li>Relaxed work atmosphere</li>
            </ul>
        </div>
        <div class="col-sm-12 col-md-1"></div>
        <div class="col-sm-12 col-md-6">
            @include('pages.application.partials.entry-form')
        </div>
    </div>
</div>



@stop

