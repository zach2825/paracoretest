@extends('layouts.default')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <h1 class="bg-danger">Uh oh!</h1>
            <p>Something went wrong and we didn't get the response we were looking for... Don't give up! Keep trying and we sure you'll figure it out.</p>
            <p>If things start looking grim, fear not! Upload your work to a repository (GitHub, BitBucket, whatever works for you) and shoot us a link at <b>devs@paracore.com</b>. We'd still be interesting in hearing from you!</p>
        </div>
    </div>
</div>

@stop