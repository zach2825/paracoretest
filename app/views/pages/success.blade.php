@extends('layouts.default')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <h1 class="bg-success">Success!</h1>
            <p>Congratulations! It appears everything worked and you're application has been submitted. Good job, and expect to hear from us soon!</p>
            <p>We have your resume, but we'd like to get one more thing from you. Now that you've completed the app, go ahead and drop it up on a repository and send us a link to it. GitHub, BitBucket, whatever works for you. Shoot an email with the link to your repository to <b>devs@paracore.com</b>.</p>
        </div>
    </div>
</div>

@stop