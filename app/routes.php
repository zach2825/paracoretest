<?php

// A return false? That looks problematic, better kill it.
// return false;

Route::get('/', [
    'as' => 'home',
    'uses' => 'ApplicationController@index'
]);

Route::post('/', [
    'as' => 'home',
    'uses' => 'ApplicationController@post'
]);