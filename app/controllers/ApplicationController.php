<?php

use GuzzleHttp\Post\PostFile;

class ApplicationController extends BaseController {

    public function index()
    {
        return View::make('pages.application.index');
    }

    public function post()
    {
        $file = Input::file('attachment');
        $formData = Input::only('name', 'email', 'comments');

        $client = new GuzzleHttp\Client();
        $request = $client->createRequest('POST', 'http://submissions.paracore.com/submission');
        $postBody = $request->getBody();
        foreach($formData as $key => $value){
            $postBody->setField($key, $value);
        }
        $postBody->addFile(new PostFile('attachment', fopen($file->getRealPath(), 'r')));
        $response = $client->send($request);


        if ($response->getStatusCode() == 200){
            return Redirect::route('success_path');
        }
        return Redirect::route('failure_path');
    }

}